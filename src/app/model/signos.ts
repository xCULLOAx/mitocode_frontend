import { Patient } from "./patient";

export class Signos{
//export interface Patient{    
    idSignos: number;
    fecha: string;
    temperatura: string;
    pulso: string;
    ritmo: string; 
    idPatient: number;
    patient: Patient;
}