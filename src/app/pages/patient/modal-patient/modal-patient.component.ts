import { Component, Inject, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { switchMap } from 'rxjs';
import { Patient } from 'src/app/model/patient';
import { PatientService } from 'src/app/service/patient.service';
import { ServicioModalPatientService } from 'src/app/service/servicio-modal-patient.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modal-patient',
  templateUrl: 'modal-patient.component.html'
})
export class ModalPatientComponent implements OnInit {
  title: string;
  @Output() onSavePatient = new EventEmitter<any>();

  constructor(
    public dialogRef: MatDialogRef<ModalPatientComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Patient,
    private dialog: MatDialog,
    private patientService: PatientService,
    private servicioModalPatientService: ServicioModalPatientService
  ) {}

  ngOnInit() {
    this.title = 'Título del diálogo';
    // this.dialogRef.close();
  }

  cancelar() {
    this.dialogRef.close();
  }

  guardar() {
     const patient = new Patient();
    patient.idPatient = 0;
    patient.firstName = this.data.firstName;
    patient.lastName = this.data.lastName;
    patient.dni = this.data.dni;
    patient.address = this.data.address;
    patient.phone = this.data.phone;
    patient.email = this.data.email; 
    
    this.patientService.save(patient).pipe(switchMap( ()=> {
      return this.patientService.findAll();
    }))
    .subscribe(data => {
      this.patientService.setPatientChange(data);
      this.ObteniendoPatient(data);
      Swal.fire(
        '¡Proceso Existoso!',
        'Recuerde, escribir nuevamente el paciente agregado.',
        'success'
      )
    });
  }

  ObteniendoPatient(data:any){     
    this.servicioModalPatientService.dispadorModalPatient.emit({
      data:data
    });
  }
}
