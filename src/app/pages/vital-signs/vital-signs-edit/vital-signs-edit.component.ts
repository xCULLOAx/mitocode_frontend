import { Component, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SignosService } from '../../../service/signos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Signos } from 'src/app/model/signos';
import { Observable, Subject, map, startWith, switchMap } from 'rxjs';
import { Patient } from 'src/app/model/patient';
import { PatientService } from 'src/app/service/patient.service';
import Swal from 'sweetalert2';
import { ModalPatientComponent } from '../../patient/modal-patient/modal-patient.component';
import { MatDialog } from '@angular/material/dialog';
import { ServicioModalPatientService } from 'src/app/service/servicio-modal-patient.service';
import * as moment from 'moment';

@Component({
  selector: 'app-vital-signs-edit',
  templateUrl: './vital-signs-edit.component.html',
  styleUrls: ['./vital-signs-edit.component.css']
})
export class VitalSignsEditComponent {
  @ViewChild(ModalPatientComponent) modalPatient: ModalPatientComponent;

  showModal = true;
  id: number;
  isEdit: boolean;
  form: FormGroup;
  patients: Patient[];
  signos: Signos[];
  // patientsFiltered$: Observable<Patient[]>;
  patientsFiltered$: Subject<Patient[]> = new Subject<Patient[]>();
  patientControl: FormControl = new FormControl();
  selectedPatientId: number;
  minDate: Date = new Date();
  dateSelected: Date;

  constructor(
    private patientService: PatientService,
    private signosService: SignosService,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    private servicioModalPatientService: ServicioModalPatientService
  ){    
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      patient: this.patientControl,
      idSignos: new FormControl(0),
      temperatura: new FormControl('', [Validators.required]),
      pulso: new FormControl('' , [Validators.required]),
      ritmo: new FormControl('', [Validators.required]),
      idPatient: new FormControl(0),
    });

    this.route.params.subscribe( data => {
      this.id = data['id'];
      this.isEdit = data['id'] != null;
      this.initForm();
    });
    this.loadInitialData();

    
    this.patientService.findAll().subscribe(data => {
      this.patients = data;
    });
    this.patientsFiltered$.next(this.patients); // Emisión inicial de todos los pacientes
    this.patientControl.valueChanges.pipe(
      map(val => this.filterPatients(val))
    ).subscribe(filteredPatients => {
      this.patientsFiltered$.next(filteredPatients); // Emisión de pacientes filtrados
    });
  

    //ESCHANDO EL EVENTO EMIT.
    this.servicioModalPatientService.dispadorModalPatient.subscribe(data => {      
        console.log("RECIBIENDO DATA: " +data);
        this.patientService.findAll().subscribe(patients => {
          this.patients = patients;
          this.patientControl.setValue(''); // resetear valor del control de pacientes
          this.patientsFiltered$.next(this.patients); // emitir lista actualizada de pacientes
        });

    });
  
  }

  //Cargamos la info de Patients
  loadInitialData(){
    this.patientService.findAll().subscribe(data => this.patients = data);
  }

  initForm(){
    if(this.isEdit){
      
      this.signosService.findById(this.id).subscribe(data => {
        this.form = new FormGroup({
          idSignos: new FormControl(data.idSignos),
          temperatura: new FormControl(data.temperatura, [Validators.required]),
          pulso: new FormControl(data.pulso, [Validators.required]),
          ritmo: new FormControl(data.ritmo, [Validators.required]),
          fecha: new FormControl(data.fecha, [Validators.required]),
          idPatient: new FormControl(data.idPatient),
        });
        this.dateSelected = new Date(data.fecha); 
        //Obteniendo el Patient - EDITAR
        this.patientService.findById(data.patient.idPatient).subscribe(res => {
          this.patientControl.setValue(res);
        })
      });

      
    }
  }

  showPatient(val: any){
    return val ? `${val.firstName} ${val.lastName}` : val;
  }

  saveForm(){

    if(this.form.invalid){
      return;
    }

    const patient = new Patient();
    patient.idPatient = this.selectedPatientId;

    const signos = new Signos();
    signos.idSignos = this.form.value['idSignos'];
    signos.temperatura = this.form.value['temperatura'];
    signos.pulso = this.form.value['pulso'];
    signos.ritmo = this.form.value['ritmo'];
    // signos.signos = this.form.value['fecha'];;
    signos.fecha = moment(this.dateSelected).format('YYYY-MM-DDTHH:mm:ss'); 
    signos.idPatient = this.selectedPatientId;
    signos.patient = patient;

    if(patient.idPatient == 0){
      Swal.fire({
        title: 'Paciente NO registrado',
        text: "Desea registrar un paciente nuevo?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, registrar!',
        cancelButtonText: 'No',
      }).then((result) => {
        if (result.value) {
        const dialogRef = this.dialog.open(ModalPatientComponent, {
          width: '600px',
          data: { /* cualquier dato que necesites pasar */ }
        });
          
        }
      })    
    }else{
      if(this.isEdit){
        this.signosService.update(signos, this.id).subscribe(data => {
          this.signosService.findAll().subscribe(data => {
            this.signosService.setSignosChange(data);
            // this.signosService.setMessageChange("UPDATED!");
            Swal.fire(
              '¡Proceso Existoso!',
              'Se actualizo correctamente el registro.',
              'success'
            )
          });
        });
      }else{
        this.signosService.save(signos).pipe(switchMap( ()=> {
          return this.signosService.findAll();
        }))
        .subscribe(data => {
          this.signosService.setSignosChange(data);
          // this.signosService.setMessageChange("CREATED!");
          Swal.fire(
            '¡Proceso Existoso!',
            'Se agrego correctamente el registro.',
            'success'
          )
        });
      }

      this.router.navigate(['/pages/signos']).then(() => {
        window.location.reload();
      });
    }
  }

  get f(){
    return this.form.controls;
  }

  filterPatients(val: any) {
    if (val?.idPatient > 0) {
      this.selectedPatientId = val.idPatient;
      return this.patients.filter(el =>
        el.firstName.toLowerCase().includes(val.firstName.toLowerCase()) || el.lastName.toLowerCase().includes(val.lastName.toLowerCase()) || el.dni.includes(val)
      )
    } else {
      this.selectedPatientId = 0;
      return this.patients.filter(el =>
        el.firstName.toLowerCase().includes(val?.toLowerCase()) || el.lastName.toLowerCase().includes(val?.toLowerCase()) || el.dni.includes(val)
      );
    }
  }

  onPatientSelected(event: any) {
    alert('Selected patient:'+event.option.value.idPatient);
  }

}
