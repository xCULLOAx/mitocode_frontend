import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { switchMap } from 'rxjs';
import { Signos } from 'src/app/model/signos';
import { SignosService } from 'src/app/service/signos.service';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-vital-signs',
  templateUrl: './vital-signs.component.html',
  styleUrls: ['./vital-signs.component.css']
})
export class VitalSignsComponent {
  displayedColumns: string[] = ['id', 'idPatient','fecha','temperatura','pulso', 'ritmo', 'actions'];
  dataSource: MatTableDataSource<Signos>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  totalElements: number;

  constructor(
    private route: ActivatedRoute,
    private signosService: SignosService,
    private _snackBar: MatSnackBar
    ){

  }

  ngOnInit(): void {
    this.signosService.getSignosChange().subscribe(data => {
      this.createTable(data);
    });

    this.signosService.getMessageChange().subscribe(data => {
      this._snackBar.open(data, 'INFO', {duration: 2000, horizontalPosition: 'right', verticalPosition: 'top'});
    });

    this.signosService.listPageable(0, 5).subscribe(data => {
      console.log(data);
      this.createTable(data);
    });

  }
  
  applyFilter(e: any){
    this.dataSource.filter = e.target.value.trim();    
  }

  createTable(data: any){
    this.dataSource = new MatTableDataSource(data.content);
    this.totalElements = data.totalElements;
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  delete(idSignos: number){

    Swal.fire({
      title: 'Eliminar',
      text: "Desea eliminar el registro seleccionado?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No',
    }).then((result) => {
      if (result.value) {
        this.signosService.delete(idSignos).pipe(
          switchMap(() => {
            return this.signosService.listPageable(this.paginator.pageIndex, this.paginator.pageSize);
          })
        ).subscribe(data => {
          this.createTable(data);
          Swal.fire(
            '¡Proceso Existoso!',
            'Se eliminó correctamente el registro.',
            'success'
          )
        });         
      }
    }) 

    
  }

  showMore(e: any){
    this.signosService.listPageable(e.pageIndex, e.pageSize).subscribe(data => {
      this.createTable(data);
    });
  }

  checkChildren(): boolean{
    return this.route.children.length != 0;
  }
}
