import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { GenericService } from './generic.service';
import { Signos } from '../model/signos';

@Injectable({
  providedIn: 'root',
})
export class SignosService extends GenericService<Signos>{
  
  private signosChange = new Subject<Signos[]>;
  private messageChange = new Subject<string>;

  constructor(protected override http: HttpClient){
    super(http, `${environment.HOST}/vsignos`)
  }

  listPageable(p: number, s: number){
    return this.http.get<any>(`${this.url}/pageable?page=${p}&size=${s}`);
  }

  setSignosChange(data: Signos[]){
    this.signosChange.next(data);
  }

  getSignosChange(){
    return this.signosChange.asObservable();
  }

  setMessageChange(data: string){
    this.messageChange.next(data);
  }

  getMessageChange(){
    return this.messageChange.asObservable();
  }
}
